#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Thu Oct 31 09:15:48 2019

@author: adtf-06
"""


import numpy as np
from matplotlib import pyplot as plt
from random import gauss



dt = 0.1
omega_sq = 2.0
X_o = 0
V_o = 1

num_steps = 400

X_of_t = np.zeros((num_steps,1))
X_of_t[0] = X_o

V_of_t = np.zeros((num_steps,1))
V_of_t[0] = V_o

X_obs = np.zeros((num_steps,1))
V_obs = np.zeros((num_steps,1))


def mioscilador_no_tan_armonico(X_o,V_o,omega_sq,dt,num_steps):
    for i in range(1,len(X_of_t)):
        V_of_t[i] = -omega_sq*X_of_t[i-1]*dt +V_of_t[i-1]
        X_of_t[i] = V_of_t[i-1]*dt+X_of_t[i-1]
        
    
#    plt.plot(X_of_t)
#    plt.plot(V_of_t)
    
    

    
    for i in range(1,len(X_of_t)):
        X_obs[i] = X_of_t[i] + gauss(0,1)
        V_obs[i] = V_of_t[i] + gauss(0,1)
        
#    plt.plot(X_obs)
#    plt.plot(V_obs)
    return X_of_t,V_of_t,X_obs,V_obs


mioscilador_no_tan_armonico(X_o,V_o,omega_sq,dt,num_steps)


X_estimado = np.zeros((num_steps,1))
V_estimado = np.zeros((num_steps,1))
omega_estimado = np.zeros((num_steps,1))


X_estimado[0] = 2
V_estimado[0] = 1
omega_estimado[0] = 1.5

num_perts = 5
particulas = np.zeros((num_steps,num_perts))
V_de_particulas = np.zeros((num_steps,num_perts))

pesos = np.zeros((num_steps,num_perts))
for i in range(num_perts):
    particulas[0,i] = X_obs[0] + gauss(0,1)
    V_de_particulas[0,i] = V_obs[0]+ gauss(0,1)
    pesos[0,i] = 1/num_perts


def adelantar_un_paso_el_oscilador(X_estimado,V_estimado,omega_sq,dt,indice_del_paso):
    V_estimado[indice_del_paso+1] = -omega_sq*X_estimado[indice_del_paso]*dt +V_estimado[indice_del_paso]
    X_estimado[indice_del_paso+1] = V_estimado[indice_del_paso]*dt+X_estimado[indice_del_paso]
    return X_estimado, V_estimado



P_o = np.array([[1,0],[0,1]])


guardador_de_P = []
guardador_de_P.append(P_o)

#    X_estimado,V_estimado = adelantar_un_paso_el_oscilador(X_estimado,V_estimado,omega_sq,dt,i-1)    
for i in range(1,num_steps):
    for j in range(num_perts):
        particula, V_de_particula = particulas[:,j],V_de_particulas[:,j]
        particula, V_de_particula = adelantar_un_paso_el_oscilador(particula,V_de_particula,omega_sq,dt,i-1)
        particulas[:,j],V_de_particulas[:,j] = particula, V_de_particula
    apartamiento = np.array([particulas[i,:]-particulas[i,:].mean(), V_de_particulas[i,:]-V_de_particulas[i,:].mean()])
    P_estimada = 1./(num_perts-1)*apartamiento.dot(apartamiento.T)
    guardador_de_P.append(P_estimada)   
    Kalman_matrix = P_estimada.dot(np.linalg.inv(np.identity(2)+P_estimada))
    for j in range(num_perts):
        estado = np.array([particulas[i,j],V_de_particulas[i,j]])
        estado = estado + Kalman_matrix.dot(np.array([X_obs[i]+gauss(0,5)-estado[0],V_obs[i]+gauss(0,5)-estado[1]])).T
        particulas[i,j],V_de_particulas[i,j] = estado[0][0],estado[0][1]



estimado = np.zeros((num_steps,1))
for i in range(num_steps):
    estimado[i] = particulas[i,:].mean()

plt.plot(X_of_t)
plt.plot(estimado)