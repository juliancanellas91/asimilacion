#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Tue Oct 22 10:14:49 2019

@author: adtf-06
"""


import numpy as np
from matplotlib import pyplot as plt
from random import gauss



dt = 0.1
omega_sq = 2.0
X_o = 0
V_o = 1
num_steps = 200


X_of_t = np.zeros((num_steps,1))
X_of_t[0] = X_o
V_of_t = np.zeros((num_steps,1))
V_of_t[0] = V_o


def mioscilador_no_tan_armonico(X_o,V_o,omega_sq,dt,num_steps):
    for i in range(1,len(X_of_t)):
        V_of_t[i] = -omega_sq*X_of_t[i-1]*dt+V_of_t[i-1]
        X_of_t[i] = V_of_t[i-1]*dt+X_of_t[i-1]
        
    
    plt.plot(X_of_t)
    plt.plot(V_of_t)
    
    
    X_obs = np.zeros((num_steps,1))
    V_obs = np.zeros((num_steps,1))
    
    for i in range(1,len(X_of_t)):
        X_obs[i] = X_of_t[i] + gauss(0,1)
        V_obs[i] = V_of_t[i] + gauss(0,1)
        
    plt.plot(X_obs)
    plt.plot(V_obs)
    return X_of_t,V_of_t


mioscilador_no_tan_armonico(X_o,V_o,omega_sq,dt,num_steps)

X_verdad = X_of_t
V_verdad = V_of_t

R = np.array([[1,0],[0,1]])
B = np.array([[1,0],[0,1]])
H = np.array([[1,0],[0,1]])

K = B.dot(H.T)*np.linalg.inv(B+(H.T).dot(R.dot(H)))

x_b = X_verdad[0] + gauss(0,1)
y_of_x = 2
y_of_v = 1



