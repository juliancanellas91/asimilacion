#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Fri Oct 18 11:03:34 2019

@author: adtf-06
"""
import numpy as np
from matplotlib import pyplot as plt

def rho(delta_x):
    L = 1
    return (1+abs(delta_x)/L)*np.exp(-abs(delta_x)/L)

alpha_list = np.array([ 0.1,0.25,0.5,1.0])
pos_2 = np.arange(-4,4,0.1)

sigma_b_sq_0 = 1
alpha = alpha_list[1]

pos_1_list = [-3,-2,-1,0]
pos_1 = pos_1_list[1]
plt.figure()
for alpha in alpha_list:
    omega_1 = (rho(pos_1)*(1+alpha)-rho(pos_2)*rho(pos_1-pos_2))/((1+alpha)**2-rho(pos_1-pos_2)**2)
    omega_2 = (rho(pos_2)*(1+alpha)-rho(pos_1)*rho(pos_1-pos_2))/((1+alpha)**2-rho(pos_1-pos_2)**2)
    sigma_a_sq_0 = sigma_b_sq_0*(1-((1+alpha)*(rho(pos_1)**2+rho(pos_2)**2)-2*rho(pos_2)*rho(pos_1-pos_2)*+rho(pos_1))/((1+alpha)**2-rho(pos_1-pos_2)**2))
#    plt.plot(pos_2,omega_1,label="Omega 1, "+"alpha " + str(alpha))
#    plt.plot(pos_2,omega_2,label="Omega 2, "+"alpha " + str(alpha))
    plt.plot(pos_2,sigma_a_sq_0,label="Sigma A, "+"alpha " + str(alpha))
    plt.legend(loc=(1,0))
plt.show()



plt.figure()
for pos_1 in pos_1_list:
    omega_1 = (rho(pos_1)*(1+alpha)-rho(pos_2)*rho(pos_1-pos_2))/((1+alpha)**2-rho(pos_1-pos_2)**2)
    omega_2 = (rho(pos_2)*(1+alpha)-rho(pos_1)*rho(pos_1-pos_2))/((1+alpha)**2-rho(pos_1-pos_2)**2)
    sigma_a_sq_0 = sigma_b_sq_0*(1-((1+alpha)*(rho(pos_1)**2+rho(pos_2)**2)-2*rho(pos_2)*rho(pos_1-pos_2)*+rho(pos_1))/((1+alpha)**2-rho(pos_1-pos_2)**2))
#    plt.plot(pos_2,omega_1,label="Omega 1, "+"pos_1 " + str(pos_1))
#    plt.plot(pos_2,omega_2,label="Omega 2, "+"pos_1 " + str(pos_1))
    plt.plot(pos_2,sigma_a_sq_0,label="Sigma A, "+"pos_1 " + str(pos_1))
    plt.legend(loc=(1,0))
plt.show()



np.linalg.eigvals([[1,0],[0,1]])