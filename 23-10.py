# -*- coding: utf-8 -*-
"""
Spyder Editor

This is a temporary script file.
"""

def resample(weights):
    """ Performs the residual resampling algorithm used by particle filters.
    Taken from pyfilt 0.1?
    Parameters:
    ----------
    weights  
    Returns:
    -------
    indexes : ndarray of ints
    array of indexes into the weights defining the resample.
    """

    N = len(weights)
    indexes = np.zeros(N, 'i')
        
    # take int(N*w) copies of each weight, which ensures particles with the
    # same weight are drawn uniformly
    num_copies = (np.floor(N*np.asarray(weights))).astype(int)
    k = 0
    for i in range(N):
        for _ in range(num_copies[i]): # make n copies
            indexes[k] = i
            k += 1
                
    # use multinormal resample on the residual to fill up the rest. 
    residual = weights - num_copies     # get fractional part
    residual /= sum(residual)           # normalize
    cumulative_sum = np.cumsum(residual)
    cumulative_sum[-1] = 1. # avoid round-off errors: ensures sum is exactly one
    ran1=np.random.uniform(size=N-k)
    indexes[k:N] = np.searchsorted(cumulative_sum, ran1)

    return indexes

import numpy as np
from random import gauss

num_steps = 200

def M(x,k):
    return 0.5*x+25*x/(1+x**2)+8*np.cos(1.2*(k-1))+gauss(0,10**(1/2))


estado_verdadero = np.zeros(num_steps)    

estado_verdadero[0] = 1

for i in range(1,len(estado_verdadero)):
    estado_verdadero[i] = M(estado_verdadero[i],i-1)
    
from matplotlib import pyplot as plt

plt.plot(estado_verdadero)

N = 10

for j in range(1,N+1):
    globals()['estado_observado%s' % j] = np.zeros(num_steps)
    globals()['estado_observado%s' % j][0] = estado_verdadero[0] + gauss(0,1) 
    
    for i in range(1,len(globals()['estado_observado%s' % j])):
        globals()['estado_observado%s' % j][i] = estado_verdadero[i] + gauss(0,1)



for j in range(1,11):
    plt.plot(globals()['estado_observado%s' % j])





particulas_iniciales = np.zeros(N)

for i in range(len(particulas_iniciales)):
    particulas_iniciales[i] = gauss(0,1)
    

#
#weights = np.zeros(len(particulas_iniciales))
#for i in range(len(weights)):
#    weights[i] = 1/(len(particulas_iniciales))

Sigma_R = 1
def H(x):
    return x

#R = np.diag([Sigma_R]*10)

def pdf(y,x,H,Sigma_R):
    return np.exp(-(y-H(x))/2*Sigma_R**2)


def SIR(particulas,observaciones,operador_observacion,distribucion,paso_de_tiempo,Sigma_R):
    weights = np.zeros(len(particulas))
    for j in range(len(particulas)):
        particulas_nuevas = M(particulas,paso_de_tiempo)
        weights[j] = distribucion(observaciones,particulas[j],operador_observacion,Sigma_R)
    F = sum(weights)
    weights = weights/F
    if ((1/sum(weights**2))/len(particulas))<0.5:
        indexs = resample(weights)
        particulas_viejas = particulas_nuevas
        for i in range(len(particulas_nuevas)):
            particulas_nuevas[i] = particulas_viejas[indexs[i]]
#            weights[i] = 1/len(particulas_nuevas)
    return particulas_nuevas, weights


particulas_iniciales = np.zeros(N)

for i in range(len(particulas_iniciales)):
    particulas_iniciales[i] = gauss(0,10)
estado_verdadero[1]
i = 1
particulas = particulas_iniciales

pesos = np.array([1/N]*10)
plt.figure()
while i<100:
    observaciones = estado_observado3[i]
    plt.plot(i,max(particulas),'b*',markersize=7)
    plt.plot(i,min(particulas),'g*')
    plt.plot(i,estado_verdadero[i],'r*')
    plt.plot(i,observaciones,'k*')
    particulas, pesos = SIR(particulas,observaciones,H,pdf,i,1)
    i += 1
plt.legend(loc=(1,0))
plt.show()







